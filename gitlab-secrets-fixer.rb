# Tool based on an earlier version by @cat:
# https://gitlab.com/gitlab-org/gitlab/-/merge_requests/21851/diffs?diff_id=67832105

# A list of [Class, (encrypted) Database column name, (decrypted) Model field name]
table_column_combos = [
  [ApplicationSetting, "runners_registration_token_encrypted", "runners_registration_token"],
  [User, "encrypted_otp_secret", "otp_secret"],
  [User, "static_object_token_encrypted", "static_object_token"],
  [GeoNode, "encrypted_secret_access_key", "secret_access_key"],
  [Group, "runners_token_encrypted", "runners_token"],
  [Project, "runners_token_encrypted", "runners_token"],
  [Ci::Runner, "token_encrypted", "token"],
  [Ci::Build, "token_encrypted", "token"],
  [Ci::Variable, "value_encrypted", "value"],
  [DeployToken, "token_encrypted", "token"],
  [GroupHook, "encrypted_url", "url"],
  [ProjectHook, "encrypted_url", "url"],
  [WebHook, "encrypted_url", "url"],
  [Operations::FeatureFlagsClient, "token_encrypted", "token"],
  [Integrations::JiraTrackerData, "encrypted_url", "url"],
  [Integrations::JiraTrackerData, "encrypted_api_url", "api_url"],
  [Integrations::JiraTrackerData, "encrypted_username", "username"],
  [Integrations::JiraTrackerData, "encrypted_password", "password"],
]

# For each identified model field/column, tests if
# they are decryptable without errors, or empties their value to NULL
table_column_combos.each do |table,column,column2|
  total = 0
  fixed = 0
  removed = 0
  bad = []
  table.find_each do |data|
    begin
      total += 1
      ::Gitlab::CryptoHelper.aes256_gcm_decrypt(data[column])
    rescue Exception => e
      if data[column2].to_s.empty?
        data[column] = nil
        data.save()
        removed += 1
      else
        data[column] = ::Gitlab::CryptoHelper.aes256_gcm_encrypt(data[column2])
        data.save()
        fixed += 1
      end
      bad << data
    end
  end
  puts "Table: #{table.name}    Bad #{bad.length} / Good #{total}, Fixed #{fixed}, Removed #{removed}"
end

# In the long term the issue https://gitlab.com/gitlab-org/gitlab/-/issues/223923
# tracks adding a formal tool as part of GitLab
