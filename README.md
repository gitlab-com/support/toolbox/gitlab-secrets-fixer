# gitlab-secrets-fixer

A tool to discover all un-decryptable secrets that cause issues at runtime and to 'fix' them by emptying their value

## Destructive behavior

This tool will clear out un-decryptable secrets. This would mean loss of stored secrets in variables, passwords, urls, etc.
depending on what the encrypted field stored originally.

While this can clear out 500 error responses, parts of the application would need to be reconfigured for effective use
again.

If you have access to the original secrets file that created the encrypted data then
it is better to replace it instead of running this tool.

## Running the tool

To use, download the script and execute it on a GitLab application host:

```
curl \
  --output /tmp/gitlab-secrets-fixer.rb \
  https://gitlab.com/gitlab-com/support/toolbox/gitlab-secrets-fixer/-/raw/main/gitlab-secrets-fixer.rb

gitlab-rails runner /tmp/gitlab-secrets-fixer.rb
```
